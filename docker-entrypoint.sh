#!/bin/bash
set -e

if [[ -z $1 ]];then
  if [[ -n ${SSL_HOST} ]];then
    python xmledit.py -u '//Service/Connector[@port="8090"]' "proxyName=${SSL_HOST}"\
                                                             "proxyPort=${SSL_PORT:-443}"\
                                                             'secure=true'\
                                                             'scheme=https'\
                      -f /app/conf/server.xml --clean
  fi
  export CATALINA_OPTS="-Dconfluence.home=${CONF_HOME} ${CATALINA_OPTS}"
  gosu java /app/bin/start-confluence.sh -fg
else
  exec $@
fi
