ARG JAVA_VERSION=8
FROM store/oracle/serverjre:${JAVA_VERSION}
ARG CONFLUENCE_VERSION=6.3.3

ENV CONF_HOME=/data \
    CONF_INSTALL=/app \
    CONFLUENCE_VERSION=${CONFLUENCE_VERSION} \
    SSL_HOST= \
    SSL_PORT=

RUN mkdir -p ${CONF_HOME}/conf/Catalina\
             ${CONF_INSTALL} \
 && yum update -y \
 && yum install -y \
               gcc \
               gzip \
               libxml2-devel \
               libxslt-devel \
               libxslt \
               python-devel \
               python-setuptools \
               tar \
 && easy_install lxml \
 && curl -o /usr/local/bin/gosu -SL 'https://github.com/tianon/gosu/releases/download/1.7/gosu-amd64' \
 && chmod +x /usr/local/bin/gosu \
 && useradd -d /app java \
 && curl -L https://atlassian.com/software/confluence/downloads/binary/atlassian-confluence-${CONFLUENCE_VERSION}.tar.gz \
  | tar xzf - --strip 1 -C /app \
 && chown -R java:java ${CONF_INSTALL}/{conf,work,logs,temp} ${CONF_HOME} \
 && yum remove -y \
               gcc \
               gzip \
               libxml2-devel \
               libxslt-devel \
               python-devel \
               python-setuptools \
               tar \
 && yum reinstall -y \
               filesystem \
 && yum autoremove -y \
 && yum clean all

VOLUME "${CONF_HOME}" \
       "${CONF_INSTALL}/logs"

EXPOSE 8090 8091

COPY "docker-entrypoint.sh" \
     "docker-test.sh" \
     "xmledit.py" \
     "/"
ENTRYPOINT ["/docker-entrypoint.sh"]
